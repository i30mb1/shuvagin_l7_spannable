package com.example.spannable

import android.content.SharedPreferences
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.BulletSpan
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.text.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val bulletSpan = BulletSpan()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val spannableStringBuilder: SpannableStringBuilder = SpannableStringBuilder("Spannable begining\n")
            .bold { scale(1.2f) {append("Bold text here (2f proportion)\n")} }
            .italic { append("Italic text here\n") }
            .bold { italic { underline { append("WTF HERE?!\n") } } }
            .backgroundColor(ContextCompat.getColor(this, android.R.color.holo_blue_bright)) { append(" BLUE LAGUNA\n") }
            .color(ContextCompat.getColor(this, android.R.color.holo_orange_light)) { append(" sun shine color ") }
            .subscript { append("5") }
            .superscript { append("5\n").setSpan(BulletSpan(20, ContextCompat.getColor(this@MainActivity,android.R.color.holo_red_dark)),0,6,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE) }
            .scale(2.5f){append("2.5f TEXT SCALE\n")}
            .strikeThrough { scale(2.2f){append("strike THROUGH FLASH")} }

        tv_activity_main.text = spannableStringBuilder
    }
}
