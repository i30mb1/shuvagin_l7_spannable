# Spannable
## Tech-stack
* [Android KTX](https://developer.android.com/kotlin/ktx) - Android KTX is a set of Kotlin extensions that are included with Android Jetpack.
* [Spannable](https://developer.android.com/reference/android/text/Spannable?hl=en) -  Change the color of a few characters, make them clickable, scale the size of the text or even draw custom bullet points with spans.

![img](screen2.png)